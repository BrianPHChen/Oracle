FROM centos:7
MAINTAINER wubm <wubm@cepave.com>

# ENV VAR
ENV HOME /root
ENV TERM xterm

# Switch to /root
WORKDIR $HOME

# Setting time zone
RUN cp /usr/share/zoneinfo/Asia/Taipei /etc/localtime

# Install package
RUN yum install -y epel-release \
    && yum install -y python34 make git net-tools go \
    python34-devel net-tools telnet bash-completion \
    wget curl vim mariadb-devel mysql

# Setting vim&bash env
RUN echo "set nu" >> /etc/vimrc \
    && echo "hi Comment ctermfg = blue" >> /etc/vimrc \
    && echo ":set expandtab" >> /etc/vimrc \
    && echo ":set tabstop=4" >> /etc/vimrc \
    && echo "parse_git_branch() {" >> /etc/bashrc \
    && echo "    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \\(.*\\)/ (\\1)/'" >> /etc/bashrc \
    && echo "}" >> /etc/bashrc

ENV PS1="\u@\h \[\033[32m\]\W\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "

# Copy files to container
ADD docker/files /tmp/files
ADD requirements.txt /tmp/requirements.txt

# Setting python3 env
RUN bash /tmp/files/python3_env.sh

# Install pip
RUN python /tmp/files/get-pip.py

# Install Oracle dependence tools
RUN pip install -r /tmp/requirements.txt

RUN rm -rf /tmp/*

EXPOSE 7788

COPY docker/docker-entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["-developer"]
