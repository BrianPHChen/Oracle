#!/bin/bash

CONF_FILE="/etc/vchain/configure.conf"
source $CONF_FILE

ORACLE_DIR=$VC_ORACLE_DIR
CORE_DIR=$VC_CORE_DIR
LOG_DIR=$LOG_FOLDER_IN_CONTAINER
TIME=`date`
RULE=$1

function change_setting_for_vchain_oracle {
    local setting_file="$ORACLE_DIR/oracle/.env"
    cp $ORACLE_DIR/oracle/.env.default $setting_file
    sed -i "s/^SECRET_KEY=.*/SECRET_KEY=\"$VC_ORACLE_SECRET_KEY\"/g" \
            $setting_file
    local T_PTCL=$VC_OSS_PROTOCOL
    local T_HOST=$VC_OSS_HOST
    local T_PORT=$VC_OSS_CONTAINER_PORT
    sed -i "s#^OSS_API_URL=.*#OSS_API_URL=\"$T_PTCL://$T_HOST:$T_PORT\"#g" \
            $setting_file
    local T_PTCL=$VC_SMARTCONTRACT_PROTOCOL
    local T_HOST=$VC_SMARTCONTRACT_HOST
    local T_PORT=$VC_SMARTCONTRACT_CONTAINER_PORT
    sed -i "s#^ORACLE_API_URL=.*#ORACLE_API_URL=\"$T_PTCL://$T_HOST:$T_PORT\"#g" \
            $setting_file
    sed -i "s/^ORACLE_DB=.*/ORACLE_DB=\"$DB_DATABASE_ORACLE\"/g" \
            $setting_file
    sed -i "s/^MYSQL_HOST=.*/MYSQL_HOST=\"$DATABASE_HOST\"/g" \
            $setting_file
    sed -i "s/^MYSQL_PORT=.*/MYSQL_PORT=\"$DATABASE_CONTAINER_PORT\"/g" \
            $setting_file
    sed -i "s/^MYSQL_USER=.*/MYSQL_USER=\"$DB_USER\"/g" \
            $setting_file
    sed -i "s/^MYSQL_PASSWORD=.*/MYSQL_PASSWORD=\"$DB_PASSWORD\"/g" \
            $setting_file
    sed -i "s#LOG_PATH=.*#LOG_PATH=\"$LOG_DIR\"#g" \
            $setting_file
    sed -i "s/ALLOWED_HOSTS=.*/ALLOWED_HOSTS=\".$VC_DOMAIN\"/g" \
            $setting_file
    if [ "$RULE" == "-developer" -o "$RULE" == "-d" ] ; then
        sed -i "s/SERVER_CONFIG_ENV=.*/SERVER_CONFIG_ENV=\"oracle.settings.develop\"/g" \
                $setting_file
    elif [ "$RULE" == "-production" -o "$RULE" == "-p" ] ; then
        sed -i "s/SERVER_CONFIG_ENV=.*/SERVER_CONFIG_ENV=\"oracle.settings.production\"/g" \
                $setting_file
    fi
    grep -r "go-ethereum" $ORACLE_DIR/oracle/ | awk -F":" '{print $1}' | \
            xargs sed "s#../go-ethereum/#../deps/go-ethereum/#g"
}

function change_setting_for_init {
    sed -i "s/name=\"ORACLE_CONTRACT_NAME\"/name=\"$VC_ORACLE_CONTRACT_NAME\"/g" \
            $ORACLE_DIR/init-bash/oracle_manage_shell.sh
}

function run_runtime_script {
    if [ -d $RUNTIME_FOLDER ] ; then
        for runtime_script in `find $RUNTIME_FOLDER -maxdepth 1 -type f -name "*.sh" -o -name "*.py" | sort` ; do
            local FNAME=`basename $runtime_script`
            TIME=`date`
            if [ ".sh" == `echo $FNAME | tail -c 4` ] ; then
                echo "$(date +'[%d/%b/%Y %T]') Start at $TIME " >> runtime_$FNAME.log
                bash $runtime_script $CONF_FILE 1>>$LOG_DIR/runtime_$FNAME.log 2>>$LOG_DIR/runtime_$FNAME.err.log &
            elif [ ".py" == `echo $FNAME | tail -c 4` ] ; then
                echo "$(date +'[%d/%b/%Y %T]') Start at $TIME " >> runtime_$FNAME.log
                python $runtime_script $CONF_FILE 1>>$LOG_DIR/runtime_$FNAME.log 2>>$LOG_DIR/runtime_$FNAME.err.log &
            else
                echo "$(date +'[%d/%b/%Y %T]') Start at $TIME " >> runtime_err.log
                echo "RUNTIME FAILED! ($FNAME)" >> runtime_err.log
            fi
            sleep 1
        done
    fi
}

if [ -f /initialization ] && [ `cat /initialization` == "1" ]; then
    if [ "$RULE" == "-production" -o "$RULE" == "-p" ] ; then
        TIME=`date`
        echo "$(date +'[%d/%b/%Y %T]') Start at $TIME " >> $LOG_DIR/oracle.log
        cd $ORACLE_DIR/oracle/
        exec python manage.py runserver 0.0.0.0:$VC_ORACLE_CONTAINER_PORT 1>>$LOG_DIR/oracle.log 2>>$LOG_DIR/oracle.err.log &
    elif [ "$RULE" == "-developer" -o "$RULE" == "-d" ] ; then
        TIME=`date`
        run_runtime_script
        echo "$(date +'[%d/%b/%Y %T]') Start at $TIME " >> $LOG_DIR/oracle.log
        cd $ORACLE_DIR/oracle/
        PYTHONUNBUFFERED=1 python manage.py runserver 0.0.0.0:$VC_ORACLE_CONTAINER_PORT 1>>$LOG_DIR/oracle.log 2>>$LOG_DIR/oracle.err.log &
        exec /sbin/init
    elif [ "$RULE" == "-modify" -o "$RULE" == "-m" ]; then
        change_setting_for_init
        change_setting_for_vchain_oracle
        exit 0
    else
        exec /sbin/init
    fi
else
    if [ "$RULE" == "-developer" -o "$RULE" == "-d" ] ; then
        cd $ORACLE_DIR
        TIME=`date`
        echo "$(date +'[%d/%b/%Y %T]') Install pygcointools at $TIME ">> $LOG_DIR/install.log
        pip3 install -e /tmp/pygcointools 1>>$LOG_DIR/install.log 2>>$LOG_DIR/install.err.log
        TIME=`date`
        echo "$(date +'[%d/%b/%Y %T]') Install gcoin-rpc at $TIME " >> $LOG_DIR/install.log
        pip3 install -e /tmp/gcoin-rpc 1>>$LOG_DIR/install.log 2>>$LOG_DIR/install.err.log
        cd $ORACLE_DIR/go-ethereum/
        TIME=`date`
        echo "$(date +'[%d/%b/%Y %T]') make evm at $TIME " >> $LOG_DIR/install.log
        make evm 1>>$LOG_DIR/install.log 2>>$LOG_DIR/install.err.log
        echo "$(date +'[%d/%b/%Y %T]') Install Finish " >> $LOG_DIR/install.log
    fi

    change_setting_for_init
    change_setting_for_vchain_oracle
    echo "Waiting for DB" >> $LOG_DIR/install.log
    until mysql -h"$DATABASE_HOST" -P"$DATABASE_CONTAINER_PORT" -u"$DB_USER" -p"$DB_PASSWORD" &> /dev/null
    do
        printf "." >> $LOG_DIR/install.log
        sleep 1
    done
    printf "DB done! \n" >> $LOG_DIR/install.log

    cd $ORACLE_DIR/oracle
    bash $ORACLE_DIR/init-bash/oracle_manage_makemigrations.sh 1>>$LOG_DIR/oracle.log 2>>$LOG_DIR/oracle.err.log
    echo "$(date +'[%d/%b/%Y %T]') First time start at $TIME " >> $LOG_DIR/oracle.log
    python manage.py migrate 1>>$LOG_DIR/oracle.log 2>>$LOG_DIR/oracle.err.log
    bash $ORACLE_DIR/init-bash/oracle_manage_shell.sh 1>>$LOG_DIR/oracle.log 2>>$LOG_DIR/oracle.err.log
    sleep 3

    if [ ! -f /initialization ]; then
        touch /initialization
    else
        sed -i '1,$d' /initialization
    fi
    echo "1" > /initialization
fi


