#!/bin/bash

ln -sf /usr/bin/python3.4 /usr/bin/python
sed -i "s/^#\!\/usr\/bin\/python$/#\!\/usr\/bin\/python2/g" /usr/bin/yum*
sed -i "s/^#\!\/usr\/bin\/python /#\!\/usr\/bin\/python2 /g" /usr/bin/yum*
sed -i "s/^#\! \/usr\/bin\/python$/#\! \/usr\/bin\/python2/g" /usr/libexec/urlgrabber-ext-down

