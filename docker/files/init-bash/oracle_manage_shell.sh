#!/bin/bash

python manage.py shell << EOF
from app.models import OraclizeContract
try:
  OraclizeContract.objects.get(name="ORACLE_CONTRACT_NAME").name
  print('Oracle contract name="ORACLE_CONTRACT_NAME" exist')
except:
  try:
    o = OraclizeContract.objects
    o.create(name="ORACLE_CONTRACT_NAME", interface="abi", address="0x157", byte_code="byte_code")
    print('Create oracle contract name="ORACLE_CONTRACT_NAME"')
  except:
    print('Create oracle contrac failed')

exit()
EOF
