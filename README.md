# Install
1. Requirement package: python3.4, python34-devel, pip, mysql, mariadb-devel, make, go, wget, git
2. ```git submodule update --init --recursive```
3. ```pip install -r ./requirements.txt```
4. ```cd deps/go-ethereum; make evm; cd ../../```
5. ```wget "https://gateway.ipfs.io/ipfs/QmTNN5EismWnAxMpQZjt8rYEUc7RsyGxvep2KZnMMfUqah" -O /bin/solc; chmod 755 /bin/solc```
6. ```cp oracle/.env.default oracle/.env```
7. modify oracle/.env
8. modify init-bash/oracle_manage_shell.sh
9. ```bash init-bash/oracle_manage_makemigrations.sh```
10. ```bash init-bash/oracle_manage_shell.sh```
11. ```cd oracle; python manage.py runserver 0.0.0.0:port_number```

